package com.example.aidzutakumi.gym_leader_hantei.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

// DB接続
public class DatabaseAccess extends DatabaseCommon {

    public DatabaseAccess(Context context) {
        super(context);
    }

    // select文発行
    public Cursor getSelectSqlResult(String sql, String[] bind){
        dbOpen();
        Cursor c = db.rawQuery(sql, bind);
        c.moveToFirst();
        dbClose();

        return c;
    }

    // update文発行
    public void updateTable(String db_table, ContentValues val, HashMap<String,String[]> update_hash_map){
        String where_string = null;
        String[] where_string_list = null;

        // HashMapの中にキーがある
        if (update_hash_map.size() != 0) {
            where_string_list = new String[update_hash_map.size()];
            where_string = "";

            // キーの数だけループ（拡張for文）
            int i = 0;
            for(String key : update_hash_map.keySet()){
                where_string = where_string + key + "=?"+ String.valueOf(update_hash_map.get(key)[1]);  // WHERE句に演算子も付け足す
                where_string_list[i] = String.valueOf(update_hash_map.get(key)[0]);
                i++;
            }
        }
        dbOpen();
        db.update(db_table, val, where_string, where_string_list);
        dbClose();
    }

}
