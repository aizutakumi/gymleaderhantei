package com.example.aidzutakumi.gym_leader_hantei;

import android.app.Activity;
import android.database.Cursor;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.content.ContentValues;
import java.util.HashMap;

import java.util.ArrayList;
import java.util.Random;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.example.aidzutakumi.gym_leader_hantei.database.DatabaseAccess;

public class Question extends Activity {
    private DatabaseAccess dba;
    private int quest_count;    // 質問の回数
    private ArrayList<Integer> question_list;

    final int NUM_BUTTONS = 3;  // ボタンの個数
    final int NUM_QUESTIONS = 15;    // DBにある質問数
    Button[] button_array = new Button[NUM_BUTTONS];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question);

        dba = new DatabaseAccess(this);
        LinearLayout linearlayout1 = (LinearLayout)findViewById(R.id.quest);

        int[][] resId = {
                { R.id.ans1, R.string.ans1 },
                { R.id.ans2, R.string.ans2 },
                { R.id.ans3, R.string.ans3 },
        };

        Intent intent = getIntent();
        quest_count = intent.getIntExtra("quest_count", 0);
        quest_count++;
        question_list = intent.getIntegerArrayListExtra("question_list");

        Random r = new Random();
        int question_no = r.nextInt(NUM_QUESTIONS)+1;  // ランダムな質問No

        while(question_list.contains(question_no)){   // 質問Noが重複しないまで乱数を取得
            question_no = r.nextInt(NUM_QUESTIONS)+1;
        }

        String sql = "SELECT q.question,c.choice,c.name " +
                     "FROM question q " +
                     "LEFT OUTER JOIN choice c " +
                     "ON (c.question_no=q.no)" +
                     "WHERE q.no = ?";
        Cursor c = dba.getSelectSqlResult(sql, new String[]{String.valueOf(question_no)});     // 質問と選択肢を取得

        TextView question = (TextView)findViewById(R.id.question);
        String q = c.getString(c.getColumnIndex("question"));
        question.setText(q);
        question_list.add(question_no);

        // 動的にボタンを生成
        for(int i = 0; i< NUM_BUTTONS; i++){

            button_array[i] = new Button(this);    //ボタン生成

            button_array[i].setId(resId[i][0]);    //リソースID設定
            button_array[i].setText(c.getString(c.getColumnIndex("choice")));    //ボタンテキスト設定
            button_array[i].setTag(c.getString(c.getColumnIndex("name")));
            c.moveToNext();

            //レイアウトに追加
            linearlayout1.addView(button_array[i]);

            // イベント設定
            final int final_array_int = i;
            findViewById(resId[i][0]).setOnClickListener(new OnClickListener(){
                public void onClick(View view) {
                    pointUp(button_array[final_array_int].getTag().toString());
                }
            });
        }
        c.close();
    }

    private void pointUp(String character) {
        // 選択した回答によって各ポイントがアップ
        String sql = "SELECT point FROM character_point WHERE name = ?;";
        Cursor point_c = dba.getSelectSqlResult(sql, new String[]{character});   // 現在のキャラクターのポイントを取得

        String table = "character_point";
        ContentValues val = new ContentValues();
        Integer point = point_c.getInt(point_c.getColumnIndex("point")) + 1;
        val.put("point", point);    // ポイントを加算

        HashMap<String,String[]> update_hash_map = new HashMap<>();
        update_hash_map.put("name", new String[] {character, ""});  // バインド変数となる値と演算子（AND OR）をセット

        dba.updateTable(table, val, update_hash_map);     // 加算したポイントに更新
        point_c.close();

        Intent intent;
        if (quest_count == 5) {
            intent = new Intent(Question.this, Result.class);
        } else {
            intent = new Intent(Question.this, Question.class);
            intent.putExtra("quest_count", quest_count);
            intent.putExtra("question_list", question_list);
        }
        startActivity(intent);
        finish();
    }
}
