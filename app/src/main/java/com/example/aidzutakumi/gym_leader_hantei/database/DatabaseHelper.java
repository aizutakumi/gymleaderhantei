package com.example.aidzutakumi.gym_leader_hantei.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseHelper extends DatabaseCommon {

    public DatabaseHelper(Context context) {
        super(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // テーブル作成
        // ジムリーダー
        db.execSQL("CREATE TABLE IF NOT EXISTS character( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                "name TEXT NOT NULL ," +
                "image_url TEXT NOT NULL ," +
                "description TEXT NOT NULL);");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('タケシ','takeshi'," +
                " 'ニビジムのジムリーダー。エキスパートは「いわ」。キャッチコピーは「つよくてかたいいしのおとこ」。\n真面目で実直な性格。\n名前の由来は「竹」');");
        db.execSQL("INSERT INTO character(name,image_url, description) VALUES ('カスミ','kasumi'," +
                " 'ハナダジムのジムリーダー。エキスパートは「みず」。キャッチコピーは「おてんばにんぎょ」。\n明るく活発な性格。\n名前の由来は「カスミソウ」');");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('マチス','machisu'," +
                " 'クチバジムのジムリーダー。エキスパートは「でんき」。キャッチコピーは「イナズマアメリカン」。\n元軍人の用心深い性格。\n名前の由来は「クレマチス」');");
        db.execSQL("INSERT INTO character(name,image_url, description) VALUES ('エリカ','erika'," +
                " 'タマムシジムのジムリーダー。エキスパートは「くさ」。キャッチコピーは「しぜんをあいするおじょうさま」。\nおっとりした性格だがお茶目な側面も持つ。\n名前の由来は「エリカ」');");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('ナツメ','natsume'," +
                " 'ヤマブキジムのジムリーダー。エキスパートは「エスパー」。キャッチコピーは「エスパーしょうじょ」。\n超能力者で寡黙な性格。\n名前の由来は「棗（なつめ）」');");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('キョウ','kyou'," +
                " 'セキチクジムのジムリーダー。エキスパートは「どく」。キャッチコピーは「どくのことならなんでもござれ」。\n忍者であり面倒見のいい性格。\n名前の由来は「杏」の音読み');");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('カツラ','katsura'," +
                " 'グレンジムのジムリーダー。エキスパートは「ほのお」キャッチコピーは「ねっけつクイズおやじ」。\nクイズが好きな熱血な性格。\n名前の由来は「桂」');");
        db.execSQL("INSERT INTO character(name,image_url,description) VALUES ('サカキ','sakaki'," +
                " 'トキワジムのジムリーダー。エキスパートは「じめん」。\nロケット団のボスであり悪のカリスマ。実は赤髪の息子がいる。\n名前の由来は「榊（さかき）」');");

        // ポイント
        db.execSQL("CREATE TABLE IF NOT EXISTS character_point( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                "name TEXT NOT NULL ," +
                "point INT ," +
                "FOREIGN KEY (name) REFERENCES character (name));");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('タケシ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('カスミ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('マチス',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('エリカ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('ナツメ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('キョウ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('カツラ',0);");
        db.execSQL("INSERT INTO character_point(name,point) VALUES ('サカキ',0);");

        // 質問
        db.execSQL("CREATE TABLE IF NOT EXISTS question( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                "question TEXT NOT NULL ," +
                "no INT NOT NULL);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('１日だけ賞味期限の切れたケーキがある',1);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('あまり話したことのない人と２人きりになった',2);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('明日は大事なテスト',3);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('お土産に大きな袋をもらった',4);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('買おうとしてた商品が目の前で売り切れた',5);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('昨日買った商品が今日から半額になってた',6);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('後ろに誰かいると言われたら',7);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('目の前にドアノブの付いた扉がある',8);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('ふかふかのベッドに',9);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('サボローが現れた',10);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('友人が食事をおごってくれると言っている',11);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('宝くじに当たった',12);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('真っ暗な部屋に閉じ込められた',13);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('トイレから人の手が現れた',14);");
        db.execSQL("INSERT INTO question(question,no) VALUES ('目の前に宇宙人が現れた',15);");

        // 選択肢
        db.execSQL("CREATE TABLE IF NOT EXISTS choice( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                "choice TEXT NOT NULL ," +
                "name TEXT NOT NULL ," +
                "question_no INT NOT NULL ," +
                "FOREIGN KEY (question_no) REFERENCES question (no) ," +
                "FOREIGN KEY (name) REFERENCES character (name));");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('誰かに毒味してもらう','マチス', 1);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('気にせず食べる','カスミ', 1);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('食べない','エリカ', 1);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('何か理由を作って逃げる','キョウ', 2);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('何も話さない','ナツメ', 2);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('自分から話しかける','カスミ', 2);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('なんか熱がある？','カツラ', 3);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('なんとかなるさ','カスミ', 3);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('勉強する','タケシ', 3);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('帰ってから開ける','タケシ', 4);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('とりあえず振ってみる','マチス', 4);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('その場で開封','サカキ', 4);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('しょうがない','エリカ', 5);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('怒る','マチス', 5);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('泣く','カスミ', 5);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('涙が出てきた…','エリカ', 6);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('タイミングが悪かったとあきらめる','ナツメ', 6);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('悔しがる','サカキ', 6);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('頭突き','タケシ', 7);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('疑う','マチス', 7);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('振り向く','エリカ', 7);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('もしかして引き戸？','エリカ', 8);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('押す','サカキ', 8);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('引く','キョウ', 8);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('潜る','ナツメ', 9);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('ダイブ','カスミ', 9);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('これください','サカキ', 9);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('たたかう','タケシ', 10);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('…負けた','マチス', 10);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('しかし回り込まれてしまった','エリカ', 10);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('ごちそうさまです','カスミ', 11);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('いやいやこっちが払います','キョウ', 11);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('割り勘でいいよ','カツラ', 11);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('誰かに報告する','カツラ', 12);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('これは夢だ','マチス', 12);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('誰にも言わない','ナツメ', 12);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('泣き出す','カスミ', 13);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('掃除をする','エリカ', 13);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('ぼーっと座ってる','サカキ', 13);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('握手する','タケシ', 14);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('黙って水を流す','ナツメ', 14);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('悲鳴をあげる','エリカ', 14);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('回れ右して逃げる','マチス', 15);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('話しかける','サカキ', 15);");
        db.execSQL("INSERT INTO choice(choice,name,question_no) VALUES ('先手必勝','カツラ', 15);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // DB_VERSIONが変更されていると呼ばれる

        // トランザクション開始
        db.beginTransaction();
        try{
            db.execSQL("DROP TABLE character;");
            db.execSQL("DROP TABLE character_point;");
            db.execSQL("DROP TABLE question;");
            db.execSQL("DROP TABLE choice;");
            db.setTransactionSuccessful();
            onCreate(db);
        } finally {
            //トランザクション終了
            db.endTransaction();
        }
    }

    // DBを開いてエラーがあれば出力
    public void init() {
        try {
            dbOpen();
        } catch(Exception e) {
            System.out.println(e);
        } finally {
            dbClose();
        }
    }
}
