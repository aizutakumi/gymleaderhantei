package com.example.aidzutakumi.gym_leader_hantei;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.aidzutakumi.gym_leader_hantei.database.DatabaseHelper;
import com.example.aidzutakumi.gym_leader_hantei.database.DatabaseAccess;

import java.util.ArrayList;
import java.util.HashMap;


public class Title extends Activity {

    private DatabaseAccess dba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);

        // DB作成
        DatabaseHelper dbh = new DatabaseHelper(this);
        dbh.init();
        dba = new DatabaseAccess(this);

        // 開始前にポイントのリセットを行う
        ContentValues val = new ContentValues();
        val.put("point", 0);
        HashMap<String,String[]> update_hash_map = new HashMap<>();
        dba.updateTable("character_point", val, update_hash_map);
    }

    public void questStart(View view){
        ArrayList<Integer> question_list = new ArrayList<>();

        Intent intent = new Intent(Title.this, Question.class);
        intent.putExtra("quest_count", 0);  // 何回質問されたかのカウント
        intent.putExtra("question_list", question_list);    // 一度回答した質問Noを控えて重複を避ける
        startActivity(intent);
        finish();
    }
}
