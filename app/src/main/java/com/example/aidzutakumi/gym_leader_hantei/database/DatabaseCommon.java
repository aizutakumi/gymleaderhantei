package com.example.aidzutakumi.gym_leader_hantei.database;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseCommon extends SQLiteOpenHelper {
    private static final String DB_NAME = "pokemon.db";
    private static final int DB_VERSION = 1;

    protected SQLiteDatabase db;

    public DatabaseCommon(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    // DBへの読み書きの許可
    protected void dbOpen() {
        db = this.getWritableDatabase();
    }

    // DBクローズ
    protected void dbClose() {
        db.close();
    }
}
