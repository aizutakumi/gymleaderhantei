package com.example.aidzutakumi.gym_leader_hantei;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aidzutakumi.gym_leader_hantei.database.DatabaseAccess;

public class Result extends Activity {
    private DatabaseAccess dba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        dba = new DatabaseAccess(this);

        String sql = "SELECT c.name,c.image_url,c.description,cp.point " +
                "FROM character c " +
                "LEFT OUTER JOIN character_point cp " +
                "ON (cp.name=c.name)" +
                "WHERE cp.point=(SELECT MAX(point) FROM character_point);";
        Cursor c = dba.getSelectSqlResult(sql, null);  // 一番ポイントが高いキャラを取得

        TextView gym_leader_result = (TextView)findViewById(R.id.gym_leader_result);    // 結果
        gym_leader_result.setText("君のタイプは" + c.getString(c.getColumnIndex("name")));
        ImageView gym_leader_image = (ImageView)findViewById(R.id.gym_leader_image);    // 画像
        String uri = c.getString(c.getColumnIndex("image_url"));
        int res = getResources().getIdentifier(uri, "drawable", getPackageName()); // 文字列をリソースIDに変換
        gym_leader_image.setImageResource(res);
        TextView gym_leader_description = (TextView)findViewById(R.id.gym_leader_description);  // 説明
        gym_leader_description.setText(c.getString(c.getColumnIndex("description")));
        c.close();
    }

    public void back(View view){
        Intent intent = new Intent(Result.this, Title.class);
        intent.putExtra("quest_count", 0);
        startActivity(intent);
        finish();
    }
}
